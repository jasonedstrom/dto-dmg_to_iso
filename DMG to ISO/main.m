//
//  main.m
//  DMG to ISO
//
//  Created by Jason Edstrom on 7/25/13.
//  Copyright (c) 2013 Jason Edstrom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
