//
//  Document.h
//  DMG to ISO
//
//  Created by Jason Edstrom on 7/25/13.
//  Copyright (c) 2013 Jason Edstrom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Document : NSDocument

@property (weak) IBOutlet NSTextField *tfDMG;
@property (weak) IBOutlet NSTextField *tfISO;
@property (weak) IBOutlet NSProgressIndicator *progressBar;
@property (weak) IBOutlet NSButton *btnISO;
@property (weak) IBOutlet NSButton *btnDMG;
@property (weak) IBOutlet NSButton *btnConvert;
@property (weak) IBOutlet NSTextField *lblSystem;

- (IBAction)fromDMG:(id)sender;
- (IBAction)fromISO:(id)sender;

- (IBAction)convert:(id)sender;

@end
