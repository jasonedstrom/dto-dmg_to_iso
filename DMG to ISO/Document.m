//
//  Document.m
//  DMG to ISO
//
//  Created by Jason Edstrom on 7/25/13.
//  Copyright (c) 2013 Jason Edstrom. All rights reserved.
//

#import "Document.h"

@implementation Document

@synthesize btnISO,btnDMG,btnConvert,tfISO,tfDMG,progressBar, lblSystem;

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"Document";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    // Insert code here to write your document to data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning nil.
    // You can also choose to override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
    NSException *exception = [NSException exceptionWithName:@"UnimplementedMethod" reason:[NSString stringWithFormat:@"%@ is unimplemented", NSStringFromSelector(_cmd)] userInfo:nil];
    @throw exception;
    return nil;
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    // Insert code here to read your document from the given data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning NO.
    // You can also choose to override -readFromFileWrapper:ofType:error: or -readFromURL:ofType:error: instead.
    // If you override either of these, you should also override -isEntireFileLoaded to return NO if the contents are lazily loaded.
    NSException *exception = [NSException exceptionWithName:@"UnimplementedMethod" reason:[NSString stringWithFormat:@"%@ is unimplemented", NSStringFromSelector(_cmd)] userInfo:nil];
    @throw exception;
    return YES;
}



-(void)browse:(int)whichbutton
{
    if (whichbutton == 0){
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
        
       
        //Isolate supported files
        [openDlg setAllowedFileTypes:[NSArray arrayWithObjects:@"dmg",@"DMG",nil]];
    
        // Multiple files not allowed
        [openDlg setAllowsMultipleSelection:NO];
        
        // cannot select a directory
        [openDlg setCanChooseDirectories:NO];
    
    // Display the dialog. If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSOKButton )
    {
        NSURL *DMG = [openDlg URL];
        NSString *str_dmg = [DMG relativePath];
        //NSLog(str_dmg);
        [tfDMG setStringValue:str_dmg];
        
        
        if (tfISO.stringValue.length == 0){
        NSString *str_iso = [str_dmg stringByDeletingPathExtension];
        NSMutableString *mstr_iso = [NSMutableString stringWithFormat:str_iso];
        [mstr_iso appendString:@".iso"];
        //NSLog(mstr_iso);
        
        [tfISO setStringValue:mstr_iso];
        
        [[tfISO currentEditor] moveToEndOfLine:nil];
        }
    }
    }else{
        NSSavePanel *saveDlg = [NSSavePanel savePanel];
        
        [saveDlg setAllowedFileTypes:[NSArray arrayWithObjects:@"iso",@"ISO",nil]];
        
        if ( [saveDlg runModal] == NSOKButton )
        {
            NSURL *ISO = [saveDlg URL];
            NSString *str_iso = [ISO relativePath];
            [tfISO setStringValue:str_iso];
            [[tfISO currentEditor] moveToEndOfLine:nil];
            
        }
    }
    
}

- (IBAction)fromDMG:(id)sender {
    [self browse:0];
}

- (IBAction)fromISO:(id)sender {
    [self browse:1];
}

- (IBAction)convert:(id)sender {
    [btnDMG setEnabled:false];
    [btnISO setEnabled:false];
    [btnConvert setEnabled:false];
    
    BOOL sourceValid = false;
    BOOL destinationValid = false;
    BOOL saveFileExists = false;
    
    //Filechecks
    sourceValid = [[NSFileManager defaultManager] fileExistsAtPath:[tfDMG stringValue]];
    destinationValid = [[NSFileManager defaultManager] fileExistsAtPath:[[tfISO stringValue] stringByDeletingLastPathComponent]];
    saveFileExists = [[NSFileManager defaultManager] fileExistsAtPath:[tfISO stringValue]];
    
    if (saveFileExists){
        [[NSFileManager defaultManager] removeItemAtPath:[tfISO stringValue] error:NULL];
    }
    
    //NSLog(@"Source valid: %@ %hhd",[tfDMG stringValue],sourceValid);
    //NSLog(@"Destination valid: %@ %hhd",[[tfISO stringValue] stringByDeletingLastPathComponent],destinationValid);
   
    if (sourceValid && destinationValid){
    
    if ([[[tfDMG stringValue] pathExtension]  isEqual: @"dmg"] && [[[tfISO stringValue] pathExtension]  isEqual: @"iso"]){
    NSTask *task = [[NSTask alloc] init];
    [progressBar startAnimation:task];
    [task setLaunchPath:@"/usr/bin/hdiutil"];
    
    NSArray *arguments = [NSArray arrayWithObjects:@"makehybrid", @"-iso", @"-hfs", @"-o", [tfISO stringValue], [tfDMG stringValue], nil];
    [task setArguments:arguments];
    
    NSPipe *pipe = [NSPipe pipe];
   
    [task setStandardOutput:pipe];
    
    NSFileHandle *file = [pipe fileHandleForReading];
    
    [task launch];
    [task waitUntilExit];
    [progressBar stopAnimation:task];
    
    NSData *data;
    data = [file readDataToEndOfFile];
    
    NSString *string;
    string = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog (@"grep returned:\n%@", string);
            
        } else{
            
            [lblSystem setStringValue:@"File Not The Correct Extension"];
                    }
    } else{
        
        if(!sourceValid){
        [lblSystem setStringValue:@"File Doesn't Exist"];
        [tfDMG setStringValue:@""];
        } else if (!destinationValid){
          [lblSystem setStringValue:@"Destination Doesn't Exist"];
          [tfISO setStringValue:@""];
        }
        
    }
    [btnDMG setEnabled:true];
    [btnISO setEnabled:true];
    [btnConvert setEnabled:true];
}
@end
